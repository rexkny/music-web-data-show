import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)
import Home from '@/views/Home'
import UserCollect from '@/views/UserCollect'
import CollectHeat from '@/views/CollectHeat'
import Search from '@/views/Search'
import SongSheetType from '@/views/Sheet/SongSheetType'
import SongSheetSeniority from '@/views/Sheet/SongSheetSeniority'
import SheetHeatDaily from '@/views/Sheet/SheetHeatDaily.vue'
import Comment from '@/views/Comment/Comment.vue'
import Intimacy from '@/views/Comment/Intimacy.vue'

export default new VueRouter({
    routes:[
        {
            path:'/home',
            name:'Home',
            component:Home,
        },
        {
            path:'/usercollect',
            name:'UserCollect',
            component:UserCollect
        },
        {
            path:'/collectheat',
            name:'CollectHeat',
            component:CollectHeat
        },
        {
            path:'/search',
            name:'Search',
            component:Search
        },
        {
            path:'/songsheetType',
            name:'SongSheetType',
            component:SongSheetType
        },
        {
            path:'/songsheetSeniority',
            name:'SongSheetSeniority',
            component:SongSheetSeniority
        },
        {
            path: '/sheetheatdaily',
            name: 'sheetHeatDaily',
            component: SheetHeatDaily
        },
        {
            path: '/comment',
            component: Comment
        },
        {
            path: '/intimacy',
            component: Intimacy
        },
        // 路由重定向  在项目跑起来的时候   访问/立马重定向到首页
        {
            path:'*',
            redirect:'/home'
        }
    ]
})